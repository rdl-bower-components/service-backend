Backend
=======

# Description
This service normalizes the communication with a backend. It provides
all methods defined in the CRUD standard and some more like multiget.
See the methods below for more informations.

If you really want to make a custom call, you can use the property 
`backendResource`. It provides the standard HTTP methods : 
+ create (POST)
+ update (PUT)
+ delete (DELETE)
+ get (GET)

`Warning` : Think twice before using the `backendResource`. Normally, you didn't 
need to use it. Every operation can be represented by a CRUD operation.

# Install

    bower install git@gitlab.com:rdl-bower-components/service-backend.git --save

Then add `rdl.backend` in your index.module.js

# Configuration

Add `backendServiceProvider` in the dependencies of the config function.
Then, you can set the the backend url with :

```javascript
backendServiceProvider.setBackendUrl(YOUR_URL);
```

# Methods

## add (type, data, id, additionalHeaders)
    
#### Description

Add a resource of the given `type` that contains the given `data`.

In the backend, you have to provide the route `/:type` in `POST`.

#### Parameters

###### type (`required`) :

Specify the type of the resource.

###### data (`required`) :

It is the properties of the object to add.

###### id :

If present, the header `ElasticId` will be added with the given value. If it is not 
present, the backend has to generate an id.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.add('type', {a: 'a'}).then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## delete (type, id, additionalHeaders)

#### Description

Delete the resource of the given `type` with the given `id`.

In the backend, you have to provide the route `/:type/:id` in `DELETE`.

#### Parameters

###### type (`required`) :

It is the type of the resource.

###### id (`required`) :

It is the id of the resource.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.delete('type', 'azerty').then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## get (type, id, additionalHeaders)
    
#### Description

Get the resource of the given `type` with the given `id`.

In the backend, you have to provide the route `/:type/:id` in `GET`.

#### Parameters

###### type (`required`) :

It is the type of the resource.

###### id (`required`) :

It is the id of the resource.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.get('type', 'azerty').then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## getAll (types, data, additionalHeaders)
    
#### Description

Get all the resources of the given `types`.

In the backend, you have to provide the route `/:types` in `GET`.

#### Parameters

###### type (`required`) :

It is the type of the resource.

###### data :

Additional data to send with the request. For instance, it can be useful to let 
the backend filter the results.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.delete('type').then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## multiget (type, ids, additionalHeaders)
    
#### Description

Get the list (`ids`) of resources of the given `types` with the given ids.

In the backend, you have to provide the route `/:types` in `GET`. You can 
differentiate it from the `getAll` with the presence of the `Ids` header.
The delimiter used to separate the id is `,`.
The backend has to return an array an not an object

#### Parameters

###### type (`required`) :

It is the type of the resource.

###### ids (`required`) :

It is the array of ids to get. It is a list of string.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.delete('type', ['azerty', 'qsdfgh]').then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```


## search (type, query, size, page, sortType, sortAsc, additionalHeaders)
    
#### Description

#### Parameters

Search resources of the given `types`.
In the backend, you have to provide the route `/:type/search` in `GET`.

###### type (`required`) :

It is the type of the resource.

###### query :

It is the search query. It is put in the header `SearchQuery`. 
By default the value is an null query.

###### size :

It is the number of wanted results. It is put in the header `SearchSize`. 
By default the value is `25`.

###### page :

It is the current page of the wanted results. It is put in the header `SearchPage`. 
By default the value is `0`.

###### sortType :

It is sort type of the results. It is put in the header `SortType`. 
By default the value is `_score`.

###### sortAsc :

It is sort order of the results. It is put in the header `SortAsc`. 
By default the value is `false`. `false` means descending and `true`
ascending.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.search('type', "test", 25, 1, '_score', true, {a:'b'}).then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```

## suggest (type, query, additionalHeaders)

#### Description

#### Parameters

Suggest resources of the given `types`.
In the backend, you have to provide the route `/:type/suggest` in `GET`.

###### type (`required`) :

It is the type of the resource.

###### query :

It is the search query. It is put in the header `SearchQuery`.
By default the value is an null query.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.suggest('type', "test", {a:'b'}).then((response) => {
console.log('success');
}, (error) => {
console.log('error');
});
```

## update (type, id, data, additionalHeaders)
    
#### Description

Update the resource of the given `type` and the given `id`.

In the backend, you have to provide the route `/:type/:id` in `PUT`.

#### Parameters

###### type (`required`) :

It is the type of the resource.

###### id (`required`) :

It is the id of the resource.

###### data (`required`) :

It is the new properties of the object with the given id.

###### additionalHeaders :

Additional headers to send with the request. For instance, it can be useful to let 
the backend filter the results.

#### Return value

It returns a `$promise` to let you recover the backend response with the `then` method.

#### Example

```javascript
backendService.update('type', 'azerty', {a: 'b'}).then((response) => {
    console.log('success');    
}, (error) => {
    console.log('error');    
});
```
