export class BackendService {
    constructor($injector, $resource, backendUrl) {

        this.backendUserResource = $resource(backendUrl + ':type/:action', {}, {
            create : {method: 'POST'},
            update : {method: 'PUT'},
            delete : {method: 'DELETE'},
            get : {method: 'GET'},
            getAll : {method: 'GET', isArray: true},
            multiget : {method: 'GET', isArray: true},
            search : {method: 'GET'}
        });
    }

    add (type, data, id = null, additionalHeaders = {}) {
        additionalHeaders.type = type;
        additionalHeaders.Id = id;
        return this.backendUserResource.create(additionalHeaders, data).$promise;
    }

    delete (type, id, additionalHeaders = {}) {
        additionalHeaders.type = type;
        additionalHeaders.action = id;
        return this.backendUserResource.delete(additionalHeaders, {}).$promise;
    }

    get (type, id, additionalHeaders = {}) {
        additionalHeaders.type = type;
        additionalHeaders.action = id;
        return this.backendUserResource.get(additionalHeaders, {}).$promise;
    }

    getAll (type, additionalHeaders = {}) {
        additionalHeaders.type = type;
        return this.backendUserResource.getAll(additionalHeaders, {}).$promise;
    }

    multiget(type, ids, additionalHeaders = {}) {
        let delimiter = ',';
        additionalHeaders.type = type;
        additionalHeaders.Ids = ids.join(delimiter);
        return this.backendUserResource.multiget(additionalHeaders, {}).$promise;
    }

    search (type, query = "", size = 25, page = 0, sortType = '_score', sortAsc = false, additionalHeaders = {}) {
        additionalHeaders.SearchPage = page;
        additionalHeaders.SearchSize = size;
        additionalHeaders.SortType = sortType;
        additionalHeaders.SortAsc = sortAsc;
        additionalHeaders.SearchQuery = (query === "") ? null : query;
        additionalHeaders.type = type;
        additionalHeaders.action = 'search';
        return this.backendUserResource.search(additionalHeaders).$promise;
    }

    suggest (type, query = "", additionalHeaders = {}) {
        additionalHeaders.SearchQuery = (query === "") ? null : query;
        additionalHeaders.type = type;
        additionalHeaders.action = 'suggest';
        return this.backendUserResource.get(additionalHeaders).$promise;
    }

    update (type, id, data, additionalHeaders = {}) {
        additionalHeaders.type = type;
        additionalHeaders.action = id;
        return this.backendUserResource.update(additionalHeaders, data).$promise;
    }
}

export class BackendServiceProvider {
    constructor() {
    }

    setBackendUrl(url) {
        this.backendUrl = url;
        if (url[url.length -1] !== '/') {
            this.backendUrl += '/';
        }
    }

    $get ($injector, $resource) {
        'ngInject';
        return new BackendService($injector, $resource, this.backendUrl);
    }
}

angular.module('rdl.backend', [])
    .provider('backendService', BackendServiceProvider);
