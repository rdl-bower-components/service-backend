/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	module.exports = __webpack_require__(1);


/***/ },
/* 1 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
	    value: true
	});

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	var BackendService = exports.BackendService = function () {
	    function BackendService($injector, $resource, backendUrl) {
	        _classCallCheck(this, BackendService);

	        this.backendUserResource = $resource(backendUrl + ':type/:action', {}, {
	            create: { method: 'POST' },
	            update: { method: 'PUT' },
	            delete: { method: 'DELETE' },
	            get: { method: 'GET' },
	            getAll: { method: 'GET', isArray: true },
	            multiget: { method: 'GET', isArray: true },
	            search: { method: 'GET' }
	        });
	    }

	    _createClass(BackendService, [{
	        key: 'add',
	        value: function add(type, data) {
	            var id = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
	            var additionalHeaders = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

	            additionalHeaders.type = type;
	            additionalHeaders.Id = id;
	            return this.backendUserResource.create(additionalHeaders, data).$promise;
	        }
	    }, {
	        key: 'delete',
	        value: function _delete(type, id) {
	            var additionalHeaders = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	            additionalHeaders.type = type;
	            additionalHeaders.action = id;
	            return this.backendUserResource.delete(additionalHeaders, {}).$promise;
	        }
	    }, {
	        key: 'get',
	        value: function get(type, id) {
	            var additionalHeaders = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	            additionalHeaders.type = type;
	            additionalHeaders.action = id;
	            return this.backendUserResource.get(additionalHeaders, {}).$promise;
	        }
	    }, {
	        key: 'getAll',
	        value: function getAll(type) {
	            var additionalHeaders = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

	            additionalHeaders.type = type;
	            return this.backendUserResource.getAll(additionalHeaders, {}).$promise;
	        }
	    }, {
	        key: 'multiget',
	        value: function multiget(type, ids) {
	            var additionalHeaders = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	            var delimiter = ',';
	            additionalHeaders.type = type;
	            additionalHeaders.Ids = ids.join(delimiter);
	            return this.backendUserResource.multiget(additionalHeaders, {}).$promise;
	        }
	    }, {
	        key: 'search',
	        value: function search(type) {
	            var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
	            var size = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 25;
	            var page = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;
	            var sortType = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : '_score';
	            var sortAsc = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : false;
	            var additionalHeaders = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : {};

	            additionalHeaders.SearchPage = page;
	            additionalHeaders.SearchSize = size;
	            additionalHeaders.SortType = sortType;
	            additionalHeaders.SortAsc = sortAsc;
	            additionalHeaders.SearchQuery = query === "" ? null : query;
	            additionalHeaders.type = type;
	            additionalHeaders.action = 'search';
	            return this.backendUserResource.search(additionalHeaders).$promise;
	        }
	    }, {
	        key: 'suggest',
	        value: function suggest(type) {
	            var query = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
	            var additionalHeaders = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

	            additionalHeaders.SearchQuery = query === "" ? null : query;
	            additionalHeaders.type = type;
	            additionalHeaders.action = 'suggest';
	            return this.backendUserResource.get(additionalHeaders).$promise;
	        }
	    }, {
	        key: 'update',
	        value: function update(type, id, data) {
	            var additionalHeaders = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

	            additionalHeaders.type = type;
	            additionalHeaders.action = id;
	            return this.backendUserResource.update(additionalHeaders, data).$promise;
	        }
	    }]);

	    return BackendService;
	}();

	var BackendServiceProvider = exports.BackendServiceProvider = function () {
	    function BackendServiceProvider() {
	        _classCallCheck(this, BackendServiceProvider);
	    }

	    _createClass(BackendServiceProvider, [{
	        key: 'setBackendUrl',
	        value: function setBackendUrl(url) {
	            this.backendUrl = url;
	            if (url[url.length - 1] !== '/') {
	                this.backendUrl += '/';
	            }
	        }
	    }, {
	        key: '$get',
	        value: ["$injector", "$resource", function $get($injector, $resource) {
	            'ngInject';

	            return new BackendService($injector, $resource, this.backendUrl);
	        }]
	    }]);

	    return BackendServiceProvider;
	}();

	angular.module('rdl.backend', []).provider('backendService', BackendServiceProvider);

/***/ }
/******/ ]);